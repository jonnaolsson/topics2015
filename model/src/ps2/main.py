"""
Author = Jonna Olsson
"""
from __future__ import print_function, division, absolute_import
from common.plotting import plot_simple, plot_gen
import numpy as np
import numpy.polynomial.chebyshev as cheb


def f(x):
    y = 1 / (1 + 25*x**2)
    return y


def cheb_roots(n):
    k = np.arange(1, n+1, 1)
    roots = - np.cos(((2*k - 1)*np.pi) / (2 * n))
    return roots


def compute_tj(x, degree):
    tj = np.zeros((x.shape[0], degree+1))
    tj[:, 0] = 1
    tj[:, 1] = x
    for i in range(2, degree+1):
        tj[:, i] = 2*x*tj[:, i-1] - tj[:, i-2]
    return tj


def approx(x, c):
    tj = compute_tj(x, c.shape[0]-1)
    y = np.dot(tj, c)
    return y


def cheb_function(x_plot, n):
    m = n + 1

    # Finding the roots for (n+1)th Cheb polynomial
    roots = cheb_roots(n+1)
    # ... and renormalizing them
    z_points = (roots - (-1)) / 2

    # Compute actual function values (just to know what we are looking for)
    y_points = f(z_points)
    y = f(x_plot)

    #####################
    # Home-made solution
    # Compute the Tj's
    tj = compute_tj(roots, n)

    # Now find theta
    theta = np.zeros(n+1)
    theta[0] = 1/m * np.sum(f(z_points))
    for i in range(1, theta.shape[0]):
        prod = f(z_points)*tj[:, i]
        theta[i] = 2/m * np.sum(prod)

    cheb_home_points = approx((2*z_points-1), theta)
    cheb_home_plot = approx((2*x_plot -1), theta)

    #####################
    # Verify by built-in solution
    coef = cheb.chebfit(z_points, f(z_points), n)
    cheb_bi_plot = cheb.chebval(x_plot, coef)

    # Verify that my function and built-in chebval function do the same
    cheb_bi_points = cheb.chebval(z_points, coef)
    cheb_bi_points_check = approx(z_points, coef)

    if (np.max(np.abs(cheb_bi_plot - cheb_home_plot))) > 1e-6:
        print('ERROR ERROR ERROR built-in and home-made solution differ!!!')
    #####################

    # Calculate absolute error
    error = np.abs(y - cheb_home_plot)
    # ... and the relative error function
    relative_error = np.max(error / y)

    return cheb_home_plot, error, relative_error


def main():
    # Polynomial of these orders to evaluate for plotting
    n_values = [3, 6, 9]

    x_plot = np.arange(0, 1, 0.001)
    y = f(x_plot)

    y_lines = np.zeros((len(n_values), x_plot.shape[0]))
    error = np.zeros((len(n_values), x_plot.shape[0]))
    llabs = []

    # Cheb polynomial for different n values (for plotting)
    for i_n, n in enumerate(n_values):
        y_lines[i_n, :], error[i_n, :], relative_error \
            = cheb_function(x_plot, n)
        label = '%.d order' % n
        llabs.append(label)

    # Checking the relative error for a longer string of n's
    n_vals = np.arange(3, 20, 1)
    relative_error = np.zeros((n_vals.shape[0]))
    for i_n, n in enumerate(n_vals):
        foo, foo2, relative_error[i_n] = cheb_function(x_plot, n)

    # Plotting the results
    ncols = len(n_values)
    plot_gen(x_plot, y, False, 'Function plot', 'x', 'f(x)', 'ps2/actual.pdf')

    plot_gen(x_plot, y_lines, True, 'Chebyshev polynomials', 'x', 'f(x)',
             'ps2/cheb_vs_actual.pdf', llabs=llabs, legloc='upper right',
             ncols=ncols)

    plot_gen(x_plot, error, True, 'Absolute error function $h_n(x)$', '$x$',
             '$|f(x) - \widehat{f}_n(x)|$',
             'ps2/cheb_error.pdf', llabs=llabs, legloc='upper right',
             ylim=[0, 0.11], ncols=ncols)

    plot_gen(n_vals, relative_error, False, 'Relative error function $r(n)$',
             'Order of polynomial',
             '$r(n)$',
             'ps2/cheb_rel_error.pdf',
             ylim=[0, np.max(relative_error)],
             xlim=[np.min(n_vals), np.max(n_vals)])

    print('Done!')


if __name__ == '__main__':
    main()