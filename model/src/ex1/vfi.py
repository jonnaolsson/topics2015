"""
Author = Jonna Olsson
"""
from __future__ import print_function, division, absolute_import
import numpy as np
from numba import jit


def vfi(par, k_grid, tol=1e-6, maxiter=500):
    vfun = np.zeros(par.knn)
    vfun1 = np.zeros_like(vfun)
    pfun = np.zeros(par.knn, dtype=np.uint32)

    # Precompute resources available at each point
    resources = k_grid**par.theta + (1-par.delta)*k_grid

    for it in range(maxiter):
        for ik_from in range(par.knn):
            v_max = -np.inf
            ik_max = 0
            for ik_to, kto in enumerate(k_grid):
                c = resources[ik_from] - kto
                if c < 0:
                    break
                v = np.log(c) + par.beta*vfun[ik_to]
                if v > v_max:
                    v_max = v
                    ik_max = ik_to
            vfun1[ik_from] = v_max
            pfun[ik_from] = ik_max

        eps = np.max(np.abs(vfun - vfun1))
        if eps < tol:
            msg = 'Vfun iterations needed: {:3d}, eps={:g}'
            print(msg.format(it, eps))
            break

        vfun, vfun1 = vfun1, vfun

    return vfun, pfun


def vfi_howard(par, k_grid, tol=1e-6, maxiter=500, howard=False, himp=10):
    vfun = np.zeros(par.knn)
    vfun1 = np.zeros_like(vfun)
    pfun = np.zeros(par.knn, dtype=np.uint32)

    # Precompute resources available at each point
    resources = k_grid**par.theta + (1-par.delta)*k_grid

    for it in range(maxiter):
        if not(it % himp == 0) and howard:
            for ik_from in range(par.knn):
                ik_to = pfun[ik_from]
                kto = k_grid[ik_to]
                c = resources[ik_from] - kto
                vfun1[ik_from] = np.log(c) + par.beta*vfun[ik_to]
        else:
            for ik_from in range(par.knn):
                v_max = -np.inf
                ik_max = 0
                for ik_to, kto in enumerate(k_grid):
                    c = resources[ik_from] - kto
                    if c < 0:
                        break
                    v = np.log(c) + par.beta*vfun[ik_to]
                    if v > v_max:
                        v_max = v
                        ik_max = ik_to
                vfun1[ik_from] = v_max
                pfun[ik_from] = ik_max

        eps = np.max(np.abs(vfun - vfun1))
        if eps < tol:
            msg = 'Vfun iterations needed: {:3d}, eps={:g}'
            print(msg.format(it, eps))
            break

        vfun, vfun1 = vfun1, vfun

    return vfun, pfun


def vfi_vector(par, k_grid, tol=1e-6, maxiter=500):
    vfun = np.zeros(par.knn)
    vfun1 = np.zeros_like(vfun)
    pfun = np.zeros(par.knn, dtype=np.uint32)

    # Precompute resources available at each point
    resources = k_grid**par.theta + (1-par.delta)*k_grid

    for it in range(maxiter):
        for ik_from in range(par.knn):
            c = resources[ik_from] - k_grid
            idx_valid = c >= 0
            v_temp = np.empty_like(c)
            v_temp[idx_valid] = np.log(c[idx_valid]) + par.beta*vfun[idx_valid]
            v_temp[~idx_valid] = -1e36
            vfun1[ik_from] = np.max(v_temp, axis=0)
            pfun[ik_from] = np.argmax(v_temp, axis=0)

        eps = np.max(np.abs(vfun - vfun1))
        if eps < tol:
            msg = 'Vfun iterations needed: {:3d}, eps={:g}'
            print(msg.format(it, eps))
            break

        vfun, vfun1 = vfun1, vfun

    return vfun, pfun


def vfi_vector_howard(par, k_grid, tol=1e-6, maxiter=500, howard=False, himp=10):
    vfun = np.zeros(par.knn)
    vfun1 = np.zeros_like(vfun)
    pfun = np.zeros(par.knn, dtype=np.uint32)

    # Precompute resources available at each point
    resources = k_grid**par.theta + (1-par.delta)*k_grid

    for it in range(maxiter):
        if not(it % himp == 0) and howard:
            resources_tmp = resources.reshape(resources.shape[0], 1)
            c = resources_tmp - k_grid
            vf_temp = np.tile(vfun, (vfun.shape[0], 1))
            idx_valid = c >= 0
            v_temp = np.empty_like(c)
            v_temp[idx_valid] = np.log(c[idx_valid]) + par.beta*vf_temp[idx_valid]
            v_temp[~idx_valid] = -1e36
            vfun1 = v_temp[np.arange(0, pfun.shape[0], 1), pfun]
        else:
            for ik_from in range(par.knn):
                c = resources[ik_from] - k_grid
                idx_valid = c >= 0
                v_temp = np.empty_like(c)
                v_temp[idx_valid] = np.log(c[idx_valid]) + par.beta*vfun[idx_valid]
                v_temp[~idx_valid] = -1e36
                vfun1[ik_from] = np.max(v_temp, axis=0)
                pfun[ik_from] = np.argmax(v_temp, axis=0)

        eps = np.max(np.abs(vfun - vfun1))
        if eps < tol:
            msg = 'Vfun iterations needed: {:3d}, eps={:g}'
            print(msg.format(it, eps))
            break

        vfun, vfun1 = vfun1, vfun

    return vfun, pfun


def vfi_mc(par, k_grid, tol=1e-6, maxiter=500):
    vfun = np.zeros(par.knn)
    vfun1 = np.zeros_like(vfun)
    pfun = np.zeros(par.knn, dtype=np.uint32)

    # Precompute resources available at each point
    resources = k_grid**par.theta + (1-par.delta)*k_grid

    for it in range(maxiter):
        ik_to = 0
        for ik_from in range(par.knn):
            v_max = -np.inf
            # Exploiting monotonicity: only looping through those values that
            # are weakly larger than last k_to that was found
            for ik_to in range(ik_to, par.knn):
                c = resources[ik_from] - k_grid[ik_to]
                if c < 0:
                    break
                v = np.log(c) + par.beta*vfun[ik_to]
                # Exploiting concavity: only looping through those values where
                # the vfun is still increasing - as soon as it starts
                # decreasing, break out and realize that last found was highest
                if v < v_max:
                    ik_to -= 1
                    break
                v_max = v

            vfun1[ik_from] = v_max
            pfun[ik_from] = ik_to

        eps = np.max(np.abs(vfun - vfun1))
        if eps < tol:
            msg = 'Vfun iterations needed: {:3d}, eps={:g}'
            print(msg.format(it, eps))
            break

        vfun, vfun1 = vfun1, vfun

    return vfun, pfun


def vfi_numba(par, k_grid, tol=1e-6, maxiter=500):
    vfun = np.zeros((par.knn))
    vfun1 = np.zeros_like(vfun)
    pfun = np.zeros((par.knn), dtype=np.uint32)

    # Precompute resources available at each point
    resources = k_grid**par.theta + (1-par.delta)*k_grid

    for it in range(maxiter):
        vfun1, pfun = vfi_inner_numba(par.knn, k_grid, vfun, vfun1, pfun,
                                      par.beta, resources)

        eps = np.max(np.abs(vfun - vfun1))
        if eps < tol:
            msg = 'Vfun iterations needed: {:3d}, eps={:g}'
            print(msg.format(it, eps))
            break

        vfun, vfun1 = vfun1, vfun

    return vfun, pfun


@jit
def vfi_inner_numba(knn, k_grid, vfun, vfun1, pfun, beta, resources):

    ik_to = 0
    for ik_from in range(knn):
        v_max = -np.inf
        # Exploiting monotonicity: only looping through those values that
        # are weakly larger than last k_to that was found
        for ik_to in range(ik_to, knn):
            c = resources[ik_from] - k_grid[ik_to]
            if c < 0:
                break
            v = np.log(c) + beta*vfun[ik_to]
            # Exploiting concavity: only looping through those values where
            # the vfun is still increasing - as soon as it starts
            # decreasing, break out and realize that last found was highest
            if v < v_max:
                ik_to -= 1
                break
            v_max = v

        vfun1[ik_from] = v_max
        pfun[ik_from] = ik_to

    return vfun1, pfun
