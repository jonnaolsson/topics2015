"""
Author = Jonna Olsson
"""
from __future__ import print_function, division, absolute_import

import numpy as np
from math import log as mlog


class ParamContainer:
    pass

par = ParamContainer()

# Runtime settings
par.tol = 1e-7
par.we_have_time = False

# Preference parameters
par.beta = 0.96
par.theta = 0.36
par.delta = 0.08
par.k_init = 0
par.sigma = 1

if par.sigma == 1:
    par.util = lambda c: mlog(c)
else:
    par.util = lambda c: (np.power(c, 1-par.sigma)-1) / (1-par.sigma)

# Asset grid parameters
par.k_min = 1
par.k_max = 100
par.knn = 1000
