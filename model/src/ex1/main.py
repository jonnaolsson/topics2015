"""
Author = Jonna Olsson
"""
from __future__ import print_function, division, absolute_import

import numpy as np
from ex1.params import par
from ex1.vfi import vfi, vfi_vector, vfi_vector_howard, vfi_howard
from ex1.vfi import vfi_mc, vfi_numba
from common.plotting import plot_simple
from time import perf_counter


def plot_results(k_grid, vfun, pfun, suffix):
    fnv = 'vfun_' + suffix
    fnp = 'pfun_' + suffix
    plot_simple(k_grid, vfun, fnv)
    plot_simple(k_grid, k_grid[pfun], fnp)


def main():
    end_msg = '>>> Done, total time {:.2f} sec'

    k_grid = np.logspace(np.log10(par.k_min), np.log10(par.k_max), par.knn)
    msg = '\nGrid size: {:.0f}'
    print(msg.format(par.knn))

    vfun_all = []
    pfun_all = []
    if par.we_have_time:
        print('\n>>> VFI simplest version')
        t0 = perf_counter()
        vfun, pfun = vfi(par, k_grid, tol=par.tol)
        t1 = perf_counter()
        print(end_msg.format(t1-t0))
        plot_simple(k_grid, vfun, 'vfun')
        plot_simple(k_grid, k_grid[pfun], 'pfun')

        print('\n>>> VFI simplest version (howard function)')
        t0 = perf_counter()
        vfun, pfun = vfi_howard(par, k_grid, tol=par.tol)
        vfun_all.append(vfun)
        pfun_all.append(pfun)
        t1 = perf_counter()
        print(end_msg.format(t1-t0))
        plot_results(k_grid, vfun, pfun, 'simple')

    print('\n>>> VFI vectorized')
    t0 = perf_counter()
    vfun, pfun = vfi_vector(par, k_grid, tol=par.tol)
    vfun_all.append(vfun)
    pfun_all.append(pfun)
    t1 = perf_counter()
    print(end_msg.format(t1-t0))
    plot_results(k_grid, vfun, pfun, 'vector')

    print('\n>>> VFI simple w Howard improvement')
    t0 = perf_counter()
    vfun, pfun = vfi_howard(par, k_grid, tol=par.tol, howard=True, himp=10)
    vfun_all.append(vfun)
    pfun_all.append(pfun)
    t1 = perf_counter()
    print(end_msg.format(t1-t0))
    plot_results(k_grid, vfun, pfun, 'simple_h')

    print('\n>>> VFI vector w Howard improvement')
    t0 = perf_counter()
    vfun, pfun = vfi_vector_howard(par, k_grid, tol=par.tol, howard=True, himp=10)
    vfun_all.append(vfun)
    pfun_all.append(pfun)
    t1 = perf_counter()
    print(end_msg.format(t1-t0))
    plot_results(k_grid, vfun, pfun, 'vh')

    print('\n>>> VFI monotonicity and concavity')
    t0 = perf_counter()
    vfun, pfun = vfi_mc(par, k_grid, tol=par.tol)
    vfun_all.append(vfun)
    pfun_all.append(pfun)
    t1 = perf_counter()
    print(end_msg.format(t1-t0))
    plot_results(k_grid, vfun, pfun, 'mc')

    print('\n>>> VFI monotonicity, concavity and numba')
    t0 = perf_counter()
    vfun, pfun = vfi_numba(par, k_grid, tol=par.tol)
    vfun_all.append(vfun)
    pfun_all.append(pfun)
    t1 = perf_counter()
    print(end_msg.format(t1-t0))
    plot_results(k_grid, vfun, pfun, 'mcn')

    for i in range(len(vfun_all)-1):
        vfun_1 = vfun_all[i]
        vfun_2 = vfun_all[i+1]
        diff_v = np.max(np.abs(vfun_1 - vfun_2))
        pfun_1 = pfun_all[i]
        pfun_2 = pfun_all[i+1]
        diff_p = np.max(np.abs(pfun_1 - pfun_2))
        diff_msg = '\n>>> Diff vfun: {:.5e}, Diff pfun: {:.5e}'
        print(diff_msg.format(diff_v, diff_p))


if __name__ == '__main__':
    main()