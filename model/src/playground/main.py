"""
Author = Jonna Olsson
"""
from __future__ import print_function, division, absolute_import

from common.plotting import plot_simple, plot_gen
import matplotlib.pyplot as plt
import numpy as np
import numpy.polynomial.chebyshev as cheb
from scipy.optimize import minimize, rosen, rosen_der, basinhopping
import scipy as scipy

import multiprocessing as mp
from multiprocessing.dummy import Pool
import random
import string
from itertools import repeat



def rand_string(length, number, output=None):
    """ Generates a random string of numbers, lower- and uppercase chars. """
    rand_str = ''.join(random.choice(
                        string.ascii_lowercase
                        + string.ascii_uppercase
                        + string.digits)
                   for i in range(length)) \
               + str(number)
    if output != None:
        output.put(rand_str)
    return rand_str



def func(a, b):
    return a + b


def old_main():
    output = mp.Queue()

    # Setup a list of processes that we want to run
    # processes = [mp.Process(target=rand_string, args=(5, x, output)) for x in
    #              range(4)]

    processes = []
    for x in range(4):
        p = mp.Process(target=rand_string, args=(5, x, output))
        processes.append(p)

    # Run processes
    for p in processes:
        p.start()

    # Exit the completed processes
    for p in processes:
        p.join()

    # Get process results from the output queue
    results = [output.get() for p in processes]

    print(results)

    chunks = []
    chunk1 = np.arange(1, 3, 1)
    chunk2 = np.arange(3, 5, 1)
    chunks.append(chunk1)
    chunks.append(chunk2)
    n_p = 2

    with Pool(processes=n_p) as pool:
        results = pool.starmap(func=rand_string, iterable=chunks)

    print(results)


    a_args = [1,2,3]
    second_arg = 1
    with Pool() as pool:
        L = pool.starmap(func, [(1, 1), (2, 1), (3, 1)])
        M = pool.starmap(func, zip(a_args, repeat(second_arg)))
        assert L == M


def poly(x1, x2, x3, x4, x5, diag=False):
    # x2 = 0.342

    xlb = -0.1
    xub = 1
    sigma2 = 0.0900**2
    mu = 0.015
    fxlb = 0
    fxub = 0

    y1 = 1.0
    fp2 = 0
    y3 = 0.5
    y4 = 0.2
    y5 = 0.1

    fpxlb = 26
    fpxub = 0

    # First condition is that f(xlb) = fxlb
    c1 = np.array(([1, xlb, xlb**2, xlb**3, xlb**4, xlb**5, xlb**6, xlb**7,
                    xlb**8, xlb**9]))
    # Second condition is that f(xub) = fxub
    c2 = np.array(([1, xub, xub**2, xub**3, xub**4, xub**5, xub**6, xub**7,
                    xub**8, xub**9]))
    # Third: Should integrate to 1
    c3 = np.array(([(1-xlb), (1-xlb**2)/2, (1-xlb**3)/3, (1-xlb**4)/4,
                    (1-xlb**5)/5, (1-xlb**6)/6, (1-xlb**7)/7, (1-xlb**8)/8,
                    (1-xlb**9)/9, (1-xlb**10)/10]))
    # Fourth: The mean should be right
    c4 = np.array(([(1-xlb**2)/2, (1-xlb**3)/3, (1-xlb**4)/4, (1-xlb**5)/5,
                    (1-xlb**6)/6, (1-xlb**7)/7, (1-xlb**8)/8, (1-xlb**9)/9,
                    (1-xlb**10)/10, (1-xlb**11)/11]))
    # Fifth: the variance should be right
    c5 = np.array(([(1-xlb**3)/3, (1-xlb**4)/4, (1-xlb**5)/5, (1-xlb**6)/6,
                    (1-xlb**7)/7, (1-xlb**8)/8, (1-xlb**9)/9, (1-xlb**10)/10,
                    (1-xlb**11)/11, (1-xlb**12)/12]))

    # fpx2 = 0
    c6 = np.array(([0, 1, 2*x2, 3*x2**2, 4*x2**3, 5*x2**4, 6*x2**5, 7*x2**6,
                    8*x2**7, 9*x2**8]))
    # Sixth condition f(x1) = y1
    c7 = np.array(([1, x1, x1 ** 2, x1 ** 3, x1 ** 4, x1 ** 5, x1 ** 6, x1 ** 7,
                    x1 **8, x1**9]))
    # Sixth condition f(x1) = y1
    c8 = np.array(
        ([1, x3, x3 ** 2, x3 ** 3, x3 ** 4, x3 ** 5, x3 ** 6, x3 ** 7, x3**8, x3**9]))
    c9 = np.array(
        ([1, x4, x4 ** 2, x4 ** 3, x4 ** 4, x4 ** 5, x4 ** 6, x4 ** 7, x4**8, x4**9]))
    c10 = np.array(
        ([1, x5, x5 ** 2, x5 ** 3, x5 ** 4, x5 ** 5, x5 ** 6, x5 ** 7, x5**8, x5**9]))

    # # Sixth condition is that f'(xlb) = fpxlb
    # c6 = np.array(([0, 1, 2*xlb, 3*xlb**2, 4*xlb**3, 5*xlb**4, 6*xlb**5]))
    # Seventh condition is that f'(xub) = fpxub
    # c7 = np.array(([0, 1, 2*xub, 3*xub**2, 4*xub**3, 5*xub**4, 6*xub**5]))

    z = np.stack((c1, c2, c3, c4, c5, c6, c7, c8, c9, c10))
    x = np.array(([fxlb, fxub, 1, mu, sigma2, fp2, y1, y3, y4, y5]))

    try:
        sol = np.linalg.solve(z, x)
        x = np.linspace(xlb, xub, 1000)
        y = sol[0] + sol[1]*x + sol[2]*x**2 + sol[3]*x**3 + sol[4]*x**4 + \
            sol[5]*x**5 + sol[6]*x**6 + sol[7]*x**7 + sol[8]*x**8 + sol[9]*x**9

        if diag:
            plt.plot(x, y)
        neg = y[np.where(y<0)]
        foo = - np.sum(neg)

    except:
        print('foo')
        foo = 1000

    return foo


def main():
    print('foo')

    x3 = 0.41
    x4 = 0.77
    x5 = 0.79
    x_guess = -0.092022
    x2_series = np.linspace(0.30, 0.40, 20)
    x3_series = np.linspace(0.25, 0.5, 20)
    x4_series = np.linspace(0.5, 0.8, 20)
    min_series = np.zeros((20*20*20))
    x1_values = np.copy(min_series)
    x2_values = np.copy(min_series)
    x3_values = np.copy(min_series)
    x4_values = np.copy(min_series)
    idx = 0
    min_value = 100

    for x2 in x2_series:
        for x3 in x3_series:
            for x4 in x4_series:
                foo = scipy.optimize.fmin(poly, x_guess, args=(x2, x3, x4, x5), full_output=True)
                min = foo[1]
                min_series[idx] = min
                x1_values[idx] = foo[0][0]
                x2_values[idx] = x2
                x3_values[idx] = x3
                x4_values[idx] = x4
                idx += 1
                if min < min_value or min < 1e-5:
                    min_value = min
                    poly(foo[0], x2, x3, x4, x5, diag=True)
                    x_guess = foo[0]




    print('ffo')


if __name__ == '__main__':
    main()