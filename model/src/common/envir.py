"""
Author = Jonna Olsson
"""
from __future__ import print_function, division, absolute_import


import os.path
from os.path import join

# Parameters controlling file system layout

repodir = join(os.path.expanduser('~'), 'repos', 'topics2015')
modeldir = join(repodir, 'model')

basedir = modeldir
srcdir = join(modeldir, 'src')
outdir = join(modeldir, 'tmp')
datadir = join(modeldir, 'input')
tmpdir = join(modeldir, 'tmp')
graphdir = join(modeldir, 'graphs')
resdir = join(repodir, 'results')
plotdir = join(repodir, 'plots')

# Parameters controlling parallelization
#nthreads_vfi = 4
