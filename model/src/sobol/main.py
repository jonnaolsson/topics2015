"""
Author = Jonna Olsson
"""
from __future__ import print_function, division, absolute_import

import numpy as np
import datetime
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sobol.sobol_seq import i4_sobol, i4_sobol_generate, i4_uniform, i4_bit_hi1, i4_bit_lo0, prime_ge
import sys
import traceback

def main():
    n = 50

    # Create sobol sequence for 2-dimensional hypercube
    s_sequence = i4_sobol_generate(2, n)
    plt.scatter(s_sequence[:, 0], s_sequence[:, 1])
    plt.show()

    # Create same with "true" random
    s_random = np.random.uniform(0, 1, n*2).reshape(n, 2)
    plt.scatter(s_random[:, 0], s_random[:, 1])
    plt.show()

    # Do same in 3D - first Sobol
    s_sequence = i4_sobol_generate(3, 50)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(s_sequence[:, 0], s_sequence[:, 1], s_sequence[:, 2],
                   zdir='z', s=20, c='b', depthshade=True)
    plt.show()

    # 3D with "random"
    s_random = np.random.uniform(0, 1, 150).reshape(50, 3)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(s_random[:, 0], s_random[:, 1], s_random[:, 2],
                   zdir='z', s=20, c='b', depthshade=True)
    plt.show()



if __name__ == '__main__':
    main()