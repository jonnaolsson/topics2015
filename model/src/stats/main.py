"""
Author = Jonna Olsson
"""
from __future__ import print_function, division, absolute_import

import numpy as np
import datetime
import matplotlib.pyplot as plt
import scipy
from scipy.linalg import lstsq


def main():

    test()
    np.random.seed(1234)

    ng = 10
    ni = np.array([1000, 1000])
    #ni = np.random.randint(20, 100, size=ng)
    nobs = np.sum(ni)

    x = np.random.randn(nobs, 1)
    x = np.hstack((np.ones((nobs, 1)), x))

    # y_foo = 100 + x[:,1] * 2
    # y_foo[-1] = 200 + x[-1, 1]*8
    # y = y_foo.reshape(101,1)
    y = np.zeros(nobs)
    y[:(nobs/2)] = 100 + x[:(nobs/2), 1]*2 + np.random.randn(nobs/2)
    y[(nobs/2):] = 100 + x[(nobs/2):, 1]*3 + np.random.randn(nobs/2)

    A = scipy.linalg.block_diag(*[np.repeat(1/x, x) for x in ni])

    x_bar = np.dot(A, x)
    y_bar = np.dot(A, y)

    # xx = x_bar * np.sqrt(np.diag(ni[:, None]))
    # yy = y_bar * np.sqrt(np.diag(ni[:, None]))

    w = np.sqrt(np.diag(ni))

    xx = np.dot(w, x_bar)
    yy = np.dot(w, y_bar)

    b_g, *rest = lstsq(xx, yy)
    b, *rest = lstsq(x, y)

    print(b)
    print(b_g)


def test():
    A=[[1,1,1,1],[1,1,1,1],[1,1,1,1],[1,1,1,1],[1,1,0,0]]
    B = [1,1,1,1,1]
    W = [1,2,3,4,5]
    W = np.sqrt(np.diag(W))
    Aw = np.dot(W,A)
    Bw = np.dot(B,W)
    X = np.linalg.lstsq(Aw, Bw)


if __name__ == '__main__':
    main()